{
    "id": "ba6db007-5d4b-40f1-9ae7-e91997a8c24d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "npc_hugo",
    "eventList": [
        {
            "id": "78762a6c-cd2f-4d36-abb5-842766597221",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ba6db007-5d4b-40f1-9ae7-e91997a8c24d"
        }
    ],
    "maskSpriteId": "ce7c6d11-41f5-4bb3-a12f-ebba405f791e",
    "overriddenProperties": null,
    "parentObjectId": "90401e05-0ad6-477a-aa5b-29f74710466d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}