{
    "id": "39d53757-08f1-4cb4-8c03-8b8fcf855556",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_transition",
    "eventList": [
        {
            "id": "11611d31-a7bb-42b4-a7ca-874659ee0ab8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "39d53757-08f1-4cb4-8c03-8b8fcf855556"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "352f50db-949b-4c9d-8b53-9dd5705991ac",
    "visible": true
}