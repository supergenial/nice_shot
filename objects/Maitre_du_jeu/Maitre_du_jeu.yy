{
    "id": "cf6dadfc-5ee6-4801-9767-85b0f5a6d4c7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "Maitre_du_jeu",
    "eventList": [
        {
            "id": "ec26d7bd-1c5b-4a77-8c2d-ee13c1e69535",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cf6dadfc-5ee6-4801-9767-85b0f5a6d4c7"
        },
        {
            "id": "dcf927e6-18f8-4919-a2d4-90a888ab3b87",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "cf6dadfc-5ee6-4801-9767-85b0f5a6d4c7"
        },
        {
            "id": "1fc20110-6efb-4a0a-a926-9ae13760c1f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cf6dadfc-5ee6-4801-9767-85b0f5a6d4c7"
        },
        {
            "id": "687231b1-af4e-42c9-9114-442b14691a64",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "cf6dadfc-5ee6-4801-9767-85b0f5a6d4c7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}