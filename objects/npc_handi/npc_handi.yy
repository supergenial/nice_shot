{
    "id": "53bbde73-bc76-456b-aff3-d2ec57e82f8c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "npc_handi",
    "eventList": [
        {
            "id": "64e723ff-8831-4d34-a31f-3559a308eea6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "53bbde73-bc76-456b-aff3-d2ec57e82f8c"
        },
        {
            "id": "e4ef027c-f978-480f-b823-dddf1b37be09",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "53bbde73-bc76-456b-aff3-d2ec57e82f8c"
        }
    ],
    "maskSpriteId": "ce7c6d11-41f5-4bb3-a12f-ebba405f791e",
    "overriddenProperties": null,
    "parentObjectId": "90401e05-0ad6-477a-aa5b-29f74710466d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}