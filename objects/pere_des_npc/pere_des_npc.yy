{
    "id": "90401e05-0ad6-477a-aa5b-29f74710466d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "pere_des_npc",
    "eventList": [
        {
            "id": "8ad992ae-34e8-45d7-a5da-92a5c9b148c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "90401e05-0ad6-477a-aa5b-29f74710466d"
        },
        {
            "id": "795b3cc1-afb7-436e-a171-5ac5ce263178",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "90401e05-0ad6-477a-aa5b-29f74710466d"
        },
        {
            "id": "474b5eb2-bfef-4227-ba93-7949fd2acfc3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "90401e05-0ad6-477a-aa5b-29f74710466d"
        },
        {
            "id": "ad5bc3fc-9162-4658-8f42-f2ab4ddb856f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "90401e05-0ad6-477a-aa5b-29f74710466d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "1d120b50-6ff4-4a6d-ab07-4d048ad93047",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}