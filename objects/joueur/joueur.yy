{
    "id": "f9146b7d-205e-4fce-8821-ded885b3e4cd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "joueur",
    "eventList": [
        {
            "id": "a69838f6-d839-4db3-9254-69c3bd748c7d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f9146b7d-205e-4fce-8821-ded885b3e4cd"
        },
        {
            "id": "e1b2fef1-1c55-4163-b347-01e5511bebe0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f9146b7d-205e-4fce-8821-ded885b3e4cd"
        },
        {
            "id": "c26c333f-a2ba-44f3-bbda-65dcd600f2dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f9146b7d-205e-4fce-8821-ded885b3e4cd"
        }
    ],
    "maskSpriteId": "ce7c6d11-41f5-4bb3-a12f-ebba405f791e",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}