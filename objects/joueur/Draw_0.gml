/// @description Insérez la description ici
// Vous pouvez écrire votre code dans cet éditeur
var anim_length = 9;
var frame_size = 64;
var anim_speed = 12;

if		(moveX < 0) y_frame=9;
else if (moveX > 0) y_frame=11;
else if (moveY < 0) y_frame=8;
else if (moveY > 0) y_frame=10;
else				x_frame=0;

var xx = x-x_offset;
var yy = y-y_offset;

if(x_frame+(anim_speed/60) <  anim_length -1) { x_frame += anim_speed/60; }
else						  { x_frame =1; }


//ombre

if(spr_shadow != -1 )draw_sprite(spr_shadows, 0, x,y);
//Dessiner la base du perso 
if(spr_base != -1 )draw_sprite_part(spr_base, 0, floor(x_frame)*frame_size, y_frame*frame_size, frame_size, frame_size, xx, yy);

//Dessiner les cheveux du perso 
if(spr_hair != -1 )draw_sprite_part(spr_hair, 0, floor(x_frame)*frame_size, y_frame*frame_size, frame_size, frame_size, xx, yy);


//Dessiner le torse du perso 
if(spr_torso != -1 )draw_sprite_part(spr_torso, 0, floor(x_frame)*frame_size, y_frame*frame_size, frame_size, frame_size, xx, yy);


//Dessiner les jambes du perso 
if(spr_legs != -1 )draw_sprite_part(spr_legs, 0, floor(x_frame)*frame_size, y_frame*frame_size, frame_size, frame_size, xx, yy);


//Dessiner les pieds du perso 
if(spr_feet != -1 )draw_sprite_part(spr_feet, 0, floor(x_frame)*frame_size, y_frame*frame_size, frame_size, frame_size, xx, yy);



