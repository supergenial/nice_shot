/// @description Insérez la description ici
// Vous pouvez écrire votre code dans cet éditeur

moveX=0;
moveY=0;



input_left   = keyboard_check(vk_left);
input_right  = keyboard_check(vk_right);
input_up     = keyboard_check(vk_up);
input_down   = keyboard_check(vk_down);
input_sprint = keyboard_check(vk_control);
input_marche = keyboard_check(vk_shift);

//changement de vitesse
if		(input_marche)   { spd=m_spd; }
else if (input_sprint)	 { spd=s_spd; }
else					 { spd=n_spd; }
//boucle de mouvement v2
moveY = (input_down - input_up) *spd;
if(moveY==0) { moveX = (input_right - input_left) *spd; }


var inst = instance_place(x,y,obj_transition);
if(inst!=noone){
	with(Maitre_du_jeu){
	spawnRoom=inst.targetRoom;
	spawnX = inst.targetX;
	spawnY = inst.targetY;
	}
  room_goto(inst.targetRoom);	
}

//Collision horizontale
if(place_meeting(x+moveX, y, object1)){
	repeat(abs(moveX)){
		if(!place_meeting(x+sign(moveX), y, object1))  { x+=sign(moveX); }
			else { break; }
	}
	moveX=0;
}


//collision verticale
if(place_meeting(x, y+moveY, object1)){
	repeat(abs(moveY)){
		if(!place_meeting(x, y+sign(moveY), object1))  { y+=sign(moveY); }
			else { break; }
	}
	moveY=0;
}

//appliquer le mouvement
x += moveX;
y += moveY;