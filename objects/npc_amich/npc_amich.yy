{
    "id": "1575d1ac-9e20-41df-a918-76da240d1704",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "npc_amich",
    "eventList": [
        {
            "id": "69b0c9ef-d181-4326-b180-ed3bbbd67a8c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1575d1ac-9e20-41df-a918-76da240d1704"
        }
    ],
    "maskSpriteId": "ce7c6d11-41f5-4bb3-a12f-ebba405f791e",
    "overriddenProperties": null,
    "parentObjectId": "90401e05-0ad6-477a-aa5b-29f74710466d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}