{
    "id": "d1fcf21d-05f2-4b14-af35-fb0a933a1461",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_female_hair_pixie_blonde",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1343,
    "bbox_left": 19,
    "bbox_right": 809,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "76b858fb-b6c1-4f54-b865-86dbf84dc3be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1fcf21d-05f2-4b14-af35-fb0a933a1461",
            "compositeImage": {
                "id": "8e9b5bf4-7725-4e97-882c-9bdff837619f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76b858fb-b6c1-4f54-b865-86dbf84dc3be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25933377-30f7-4e71-82da-6fe4a33432ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76b858fb-b6c1-4f54-b865-86dbf84dc3be",
                    "LayerId": "809f8466-bead-4559-80fc-1353310ac26a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "809f8466-bead-4559-80fc-1353310ac26a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d1fcf21d-05f2-4b14-af35-fb0a933a1461",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}