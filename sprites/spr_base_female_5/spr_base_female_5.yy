{
    "id": "9d050f2e-6fd2-4da0-82bf-6d3d8e59b301",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_base_female_5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1342,
    "bbox_left": 18,
    "bbox_right": 818,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "92fc3386-760b-4d30-b664-7abb420c6877",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d050f2e-6fd2-4da0-82bf-6d3d8e59b301",
            "compositeImage": {
                "id": "b1e55bd6-6bce-465d-8a61-644d42ea80e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92fc3386-760b-4d30-b664-7abb420c6877",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5bcc533b-a7ec-4353-9901-017dd2a6f1cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92fc3386-760b-4d30-b664-7abb420c6877",
                    "LayerId": "2f1bb9fe-1f1e-4bac-b6e7-a513f3e101a1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "2f1bb9fe-1f1e-4bac-b6e7-a513f3e101a1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9d050f2e-6fd2-4da0-82bf-6d3d8e59b301",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}