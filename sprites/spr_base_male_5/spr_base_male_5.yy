{
    "id": "4e25a522-5ee9-43d5-a30c-29b9e49331f7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_base_male_5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1342,
    "bbox_left": 16,
    "bbox_right": 818,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "576f706f-c8b3-4f7a-bf88-ab645a1ba8a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e25a522-5ee9-43d5-a30c-29b9e49331f7",
            "compositeImage": {
                "id": "39383262-ca5d-40de-88d0-62e48fc610d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "576f706f-c8b3-4f7a-bf88-ab645a1ba8a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ebd3cdd-eb92-47db-8e42-2a342dd7346a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "576f706f-c8b3-4f7a-bf88-ab645a1ba8a1",
                    "LayerId": "622a5e42-ba2f-41c9-aa15-daf61840534d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "622a5e42-ba2f-41c9-aa15-daf61840534d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4e25a522-5ee9-43d5-a30c-29b9e49331f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}