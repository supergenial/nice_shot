{
    "id": "d07931ac-0840-4956-b04a-e67f8f14a866",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_legs_male_pants_green",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1340,
    "bbox_left": 22,
    "bbox_right": 552,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f8206731-2719-4ada-969f-cc5c39fc7a0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d07931ac-0840-4956-b04a-e67f8f14a866",
            "compositeImage": {
                "id": "073ad0d7-00ca-46c8-ae69-cb29be1eafe5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8206731-2719-4ada-969f-cc5c39fc7a0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79af8100-bebf-41f0-891b-e33add4c61d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8206731-2719-4ada-969f-cc5c39fc7a0b",
                    "LayerId": "8a2f0415-e4f6-4df2-a694-c95329a0d569"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "8a2f0415-e4f6-4df2-a694-c95329a0d569",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d07931ac-0840-4956-b04a-e67f8f14a866",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}