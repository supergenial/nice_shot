{
    "id": "187ec381-589b-4c79-a15e-980d1c8e799d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_female_torso_sleeveless_red",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1336,
    "bbox_left": 23,
    "bbox_right": 807,
    "bbox_top": 34,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b7a96967-76e8-46f9-a89c-8f2856d2e13c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "187ec381-589b-4c79-a15e-980d1c8e799d",
            "compositeImage": {
                "id": "b9ad0cc4-3878-4fbe-8749-61a26e443b9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7a96967-76e8-46f9-a89c-8f2856d2e13c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a34a79e4-335c-4a4c-9c38-a68fbc204dd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7a96967-76e8-46f9-a89c-8f2856d2e13c",
                    "LayerId": "742249b5-b935-40a7-8c81-c151dffb12be"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "742249b5-b935-40a7-8c81-c151dffb12be",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "187ec381-589b-4c79-a15e-980d1c8e799d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}