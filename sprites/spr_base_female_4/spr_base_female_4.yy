{
    "id": "b1cf277b-69c8-47eb-803f-c60b42120549",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_base_female_4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1342,
    "bbox_left": 18,
    "bbox_right": 818,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cca4d95a-fb7a-44d2-99a3-2b80b5f264f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b1cf277b-69c8-47eb-803f-c60b42120549",
            "compositeImage": {
                "id": "fe3b9621-2ebc-46fe-abf1-04a41789f7ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cca4d95a-fb7a-44d2-99a3-2b80b5f264f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2109214-b0c0-4933-a44a-21ad51ca9fb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cca4d95a-fb7a-44d2-99a3-2b80b5f264f3",
                    "LayerId": "d3a400f0-88c1-4560-bbd9-1218226d4937"
                }
            ]
        }
    ],
    "gridX": 64,
    "gridY": 64,
    "height": 1344,
    "layers": [
        {
            "id": "d3a400f0-88c1-4560-bbd9-1218226d4937",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b1cf277b-69c8-47eb-803f-c60b42120549",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}