{
    "id": "36988a39-b0e4-4af8-9276-da9f00bfdc9a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_base_male_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1342,
    "bbox_left": 16,
    "bbox_right": 818,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "972c2599-8716-4452-ae6c-c49956bc1bc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36988a39-b0e4-4af8-9276-da9f00bfdc9a",
            "compositeImage": {
                "id": "4ac3e8ae-5a70-4c4c-8cdf-987846426942",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "972c2599-8716-4452-ae6c-c49956bc1bc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d455e97-326e-48b8-89d7-e4235ca6bba2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "972c2599-8716-4452-ae6c-c49956bc1bc2",
                    "LayerId": "91c6eb0f-57d7-4807-9742-b62ceafe4069"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "91c6eb0f-57d7-4807-9742-b62ceafe4069",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "36988a39-b0e4-4af8-9276-da9f00bfdc9a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}