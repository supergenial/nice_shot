{
    "id": "5e795002-2940-4b6d-8eef-e6a3a5037851",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_base_female_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1342,
    "bbox_left": 18,
    "bbox_right": 818,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bb00e57d-6837-416c-a598-e677471e12d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e795002-2940-4b6d-8eef-e6a3a5037851",
            "compositeImage": {
                "id": "84f92201-bfde-4756-81ab-30ed050f6c4a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb00e57d-6837-416c-a598-e677471e12d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c49aadd-c70d-4630-a770-66c8b2c8d2c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb00e57d-6837-416c-a598-e677471e12d7",
                    "LayerId": "d8f6ab64-627a-4319-9324-ed5151557c57"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "d8f6ab64-627a-4319-9324-ed5151557c57",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5e795002-2940-4b6d-8eef-e6a3a5037851",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}