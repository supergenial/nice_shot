{
    "id": "5967bf53-4996-4637-85df-77ff285a2f5a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tile_terrain",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1023,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1c092bce-ad36-46d5-8227-0e027b2a4530",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5967bf53-4996-4637-85df-77ff285a2f5a",
            "compositeImage": {
                "id": "6b55de89-6c3c-4f17-ac8f-87f79725ee5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c092bce-ad36-46d5-8227-0e027b2a4530",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f759381-2f2c-425e-9b3d-710e0b47acd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c092bce-ad36-46d5-8227-0e027b2a4530",
                    "LayerId": "c4960c7e-6bfd-4d28-ad5f-0ff287fa62c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1024,
    "layers": [
        {
            "id": "c4960c7e-6bfd-4d28-ad5f-0ff287fa62c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5967bf53-4996-4637-85df-77ff285a2f5a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}