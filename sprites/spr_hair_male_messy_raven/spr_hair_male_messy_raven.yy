{
    "id": "51a626ee-aa64-4a13-bae6-0d56f46b1397",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_hair_male_messy_raven",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1343,
    "bbox_left": 18,
    "bbox_right": 812,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d1936bca-c7fa-491f-b1f3-7e2dafc00dac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51a626ee-aa64-4a13-bae6-0d56f46b1397",
            "compositeImage": {
                "id": "4e17dbf3-6755-43f2-bd31-2d10e9ba492e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1936bca-c7fa-491f-b1f3-7e2dafc00dac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64daad08-9665-4d08-8a80-fe4d9b43a7b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1936bca-c7fa-491f-b1f3-7e2dafc00dac",
                    "LayerId": "bd8c2900-fb4e-412c-9d9a-d6add46d2972"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "bd8c2900-fb4e-412c-9d9a-d6add46d2972",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "51a626ee-aa64-4a13-bae6-0d56f46b1397",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}