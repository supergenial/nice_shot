{
    "id": "0151a862-7a0a-44ff-bf46-49e03f50dd1f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_female_feet_boots_black",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1342,
    "bbox_left": 23,
    "bbox_right": 809,
    "bbox_top": 54,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f029112a-27d0-4131-9527-ae87452fbb36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0151a862-7a0a-44ff-bf46-49e03f50dd1f",
            "compositeImage": {
                "id": "dd605b9d-6f3f-48c6-968e-3590d06f1f6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f029112a-27d0-4131-9527-ae87452fbb36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fec20b4-1640-458c-a23e-7a48c3e63ee4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f029112a-27d0-4131-9527-ae87452fbb36",
                    "LayerId": "5aee2c4e-d589-4bc1-97b0-cdb0c8e8eaef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "5aee2c4e-d589-4bc1-97b0-cdb0c8e8eaef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0151a862-7a0a-44ff-bf46-49e03f50dd1f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}