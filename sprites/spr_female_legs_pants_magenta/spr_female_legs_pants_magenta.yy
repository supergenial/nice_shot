{
    "id": "2fd00edb-f035-4f15-b0e9-78bcfbbfc705",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_female_legs_pants_magenta",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1341,
    "bbox_left": 24,
    "bbox_right": 807,
    "bbox_top": 45,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "171956b7-d30d-43a0-8ed6-ee574f7d35b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2fd00edb-f035-4f15-b0e9-78bcfbbfc705",
            "compositeImage": {
                "id": "f4737c49-fac2-44ce-ac96-68febca712d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "171956b7-d30d-43a0-8ed6-ee574f7d35b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f08f43e8-eb84-404e-aba8-691b9fc4f950",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "171956b7-d30d-43a0-8ed6-ee574f7d35b5",
                    "LayerId": "b8e3abc4-5cff-42da-a57f-9523eccbc5bc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "b8e3abc4-5cff-42da-a57f-9523eccbc5bc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2fd00edb-f035-4f15-b0e9-78bcfbbfc705",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}