{
    "id": "cc624812-f56b-4252-807d-43d56c169e2b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_male_hair_bedhead_pink",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1343,
    "bbox_left": 18,
    "bbox_right": 811,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "145b439f-b9dc-40b4-85e7-66713de57ffe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc624812-f56b-4252-807d-43d56c169e2b",
            "compositeImage": {
                "id": "bbee879b-c689-49b7-b1e4-3b701ba9f043",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "145b439f-b9dc-40b4-85e7-66713de57ffe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c02ff60d-998c-491f-a17e-044b93f35495",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "145b439f-b9dc-40b4-85e7-66713de57ffe",
                    "LayerId": "d21c9844-5769-4be7-962b-3f15e0c1e73e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "d21c9844-5769-4be7-962b-3f15e0c1e73e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cc624812-f56b-4252-807d-43d56c169e2b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}