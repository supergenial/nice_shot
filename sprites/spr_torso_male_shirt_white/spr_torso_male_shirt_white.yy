{
    "id": "4c52af96-5e2d-4623-96c3-9f3950f6d511",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_torso_male_shirt_white",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1336,
    "bbox_left": 17,
    "bbox_right": 558,
    "bbox_top": 31,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "63014c8d-5f0c-4715-88f0-055c34aadf73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c52af96-5e2d-4623-96c3-9f3950f6d511",
            "compositeImage": {
                "id": "11cd828c-9f67-4c4f-b674-63b85e4be5cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63014c8d-5f0c-4715-88f0-055c34aadf73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68303953-87f2-4844-999a-900f6e75b2cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63014c8d-5f0c-4715-88f0-055c34aadf73",
                    "LayerId": "f8e510ce-34ed-4237-b4cb-fbd1cac2ffbf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "f8e510ce-34ed-4237-b4cb-fbd1cac2ffbf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4c52af96-5e2d-4623-96c3-9f3950f6d511",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}