{
    "id": "5e3bc3d7-ca22-4d0b-9d0a-34a7d2f5c412",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_base_female_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1342,
    "bbox_left": 18,
    "bbox_right": 818,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1d196992-6f70-41fc-8659-7cad9b77ecb9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e3bc3d7-ca22-4d0b-9d0a-34a7d2f5c412",
            "compositeImage": {
                "id": "903d4556-5561-45bd-ad9c-9bc4b16bcb4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d196992-6f70-41fc-8659-7cad9b77ecb9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "50e7a15a-aa69-49de-9445-f2a4ed87984d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d196992-6f70-41fc-8659-7cad9b77ecb9",
                    "LayerId": "0217134f-169e-4538-b36d-1f3f2bfa827e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "0217134f-169e-4538-b36d-1f3f2bfa827e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5e3bc3d7-ca22-4d0b-9d0a-34a7d2f5c412",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}