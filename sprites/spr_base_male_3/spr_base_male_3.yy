{
    "id": "5e2bb3cc-f5f0-4cdb-8714-ce29dd3fa40b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_base_male_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1342,
    "bbox_left": 16,
    "bbox_right": 818,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c3394881-d2e4-4017-91d3-0d993be1e754",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e2bb3cc-f5f0-4cdb-8714-ce29dd3fa40b",
            "compositeImage": {
                "id": "197ce21d-79ff-4cbf-a181-e1bfc8d1d86b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3394881-d2e4-4017-91d3-0d993be1e754",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d01fdeb7-175d-4f78-9dd1-0aae247463f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3394881-d2e4-4017-91d3-0d993be1e754",
                    "LayerId": "ebedfe73-a7ee-486b-9a6d-45da12c313f5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "ebedfe73-a7ee-486b-9a6d-45da12c313f5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5e2bb3cc-f5f0-4cdb-8714-ce29dd3fa40b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}