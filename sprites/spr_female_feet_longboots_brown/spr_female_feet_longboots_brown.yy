{
    "id": "e7dc513c-40ba-41c3-ac26-ffc08e040270",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_female_feet_longboots_brown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1342,
    "bbox_left": 23,
    "bbox_right": 809,
    "bbox_top": 50,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2c0d7786-f92a-4d34-8089-87efc0fd8558",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7dc513c-40ba-41c3-ac26-ffc08e040270",
            "compositeImage": {
                "id": "023d2ceb-1869-4b14-a0d4-c19b2195c5a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c0d7786-f92a-4d34-8089-87efc0fd8558",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6da0b54b-b976-4c7d-a6b3-24c5d13c045e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c0d7786-f92a-4d34-8089-87efc0fd8558",
                    "LayerId": "18fddd99-6b58-45b9-8edc-cb4a51f91cfe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "18fddd99-6b58-45b9-8edc-cb4a51f91cfe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e7dc513c-40ba-41c3-ac26-ffc08e040270",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}