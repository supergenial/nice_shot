{
    "id": "a1ea658b-7153-4a1f-b36b-c5d3c6cba2c4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_joueur",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "61537a89-197b-4547-9ced-a5632abfbf58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1ea658b-7153-4a1f-b36b-c5d3c6cba2c4",
            "compositeImage": {
                "id": "db702cfd-d681-4516-a234-e90c9184a417",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61537a89-197b-4547-9ced-a5632abfbf58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "200c6742-39d5-4959-a5ca-21c09324d86a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61537a89-197b-4547-9ced-a5632abfbf58",
                    "LayerId": "01e6914d-4abd-4c5f-8b7a-c0081873fad7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "01e6914d-4abd-4c5f-8b7a-c0081873fad7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a1ea658b-7153-4a1f-b36b-c5d3c6cba2c4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}