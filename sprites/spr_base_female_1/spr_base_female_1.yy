{
    "id": "a04ccaaf-0597-48ab-94b2-414c2b2bbb94",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_base_female_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1342,
    "bbox_left": 18,
    "bbox_right": 818,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c01bb557-f804-49f4-8d41-4882595563fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a04ccaaf-0597-48ab-94b2-414c2b2bbb94",
            "compositeImage": {
                "id": "48f81b98-f897-4cdc-a976-d229ffb2d18d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c01bb557-f804-49f4-8d41-4882595563fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed60df86-6806-4efe-b1a2-43beeee26d0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c01bb557-f804-49f4-8d41-4882595563fa",
                    "LayerId": "d3f1a291-bb6a-48df-8037-d6def6afa8d3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "d3f1a291-bb6a-48df-8037-d6def6afa8d3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a04ccaaf-0597-48ab-94b2-414c2b2bbb94",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}