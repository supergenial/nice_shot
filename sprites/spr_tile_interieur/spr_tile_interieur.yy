{
    "id": "1f3fb42b-e194-4902-b566-cf8e45b2b299",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tile_interieur",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 703,
    "bbox_left": 96,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8673fbe4-ae9d-4d3d-9d5c-867f08e12da2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1f3fb42b-e194-4902-b566-cf8e45b2b299",
            "compositeImage": {
                "id": "569d05a5-4264-4a62-b8ca-96f9527f1b19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8673fbe4-ae9d-4d3d-9d5c-867f08e12da2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25d2ec50-91c4-4f75-bcc3-a33e96af7a9d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8673fbe4-ae9d-4d3d-9d5c-867f08e12da2",
                    "LayerId": "5b9bc5c6-c93c-43fb-b25a-a9b90b18013b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1024,
    "layers": [
        {
            "id": "5b9bc5c6-c93c-43fb-b25a-a9b90b18013b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1f3fb42b-e194-4902-b566-cf8e45b2b299",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 512,
    "yorig": 512
}