{
    "id": "6cd5a0f1-cff5-4eed-a17b-2ffc150b4991",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_base_male_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1342,
    "bbox_left": 16,
    "bbox_right": 818,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3e563326-d70c-4d6f-a4e1-9ce910c868ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6cd5a0f1-cff5-4eed-a17b-2ffc150b4991",
            "compositeImage": {
                "id": "d68e7e1d-1926-469c-b1bf-374ac0887e5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e563326-d70c-4d6f-a4e1-9ce910c868ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73c87af2-9e00-4ad6-bd2d-18e080f3f883",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e563326-d70c-4d6f-a4e1-9ce910c868ac",
                    "LayerId": "4f3fb77f-027c-4aae-aff7-fe75b80db0d7"
                }
            ]
        }
    ],
    "gridX": 64,
    "gridY": 64,
    "height": 1344,
    "layers": [
        {
            "id": "4f3fb77f-027c-4aae-aff7-fe75b80db0d7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6cd5a0f1-cff5-4eed-a17b-2ffc150b4991",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 340,
    "yorig": 355
}