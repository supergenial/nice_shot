{
    "id": "f84a8791-1239-499e-a98e-ec699b13d3ea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_base_male_4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1342,
    "bbox_left": 16,
    "bbox_right": 818,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f57effdd-2b55-4746-93b2-35cff805679c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f84a8791-1239-499e-a98e-ec699b13d3ea",
            "compositeImage": {
                "id": "463d5760-d90f-4fa2-8636-a96b9d94d433",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f57effdd-2b55-4746-93b2-35cff805679c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb657b4a-5fde-4c79-b397-efbff4e645c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f57effdd-2b55-4746-93b2-35cff805679c",
                    "LayerId": "a5399e38-136d-45ed-a6f2-0091e2abf380"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "a5399e38-136d-45ed-a6f2-0091e2abf380",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f84a8791-1239-499e-a98e-ec699b13d3ea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}