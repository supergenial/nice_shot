{
    "id": "1b21d040-70ab-4624-970b-ec020740a862",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gore",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 925,
    "bbox_left": 0,
    "bbox_right": 762,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e09b76c5-891a-4a94-8f64-2ba0c10cefb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b21d040-70ab-4624-970b-ec020740a862",
            "compositeImage": {
                "id": "2e752178-61fe-44e1-82aa-c336a2fdb41c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e09b76c5-891a-4a94-8f64-2ba0c10cefb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cdc26bb7-21bc-40e4-927d-10ad0de48b14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e09b76c5-891a-4a94-8f64-2ba0c10cefb8",
                    "LayerId": "1c72e249-0867-4791-b3c4-80ea0483be71"
                }
            ]
        }
    ],
    "gridX": 32,
    "gridY": 32,
    "height": 1024,
    "layers": [
        {
            "id": "1c72e249-0867-4791-b3c4-80ea0483be71",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b21d040-70ab-4624-970b-ec020740a862",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 512,
    "yorig": 512
}