{
    "id": "cec93466-86a1-4f8f-8bf9-5f1d53c4a208",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tile_cottage",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "33b35d5d-a16c-4467-bea4-448e60b176df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cec93466-86a1-4f8f-8bf9-5f1d53c4a208",
            "compositeImage": {
                "id": "7f9af8cc-8ec4-46e7-b4d7-b23b97d015cb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33b35d5d-a16c-4467-bea4-448e60b176df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f8338cf-ba5e-426e-9684-fa4ece126a14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33b35d5d-a16c-4467-bea4-448e60b176df",
                    "LayerId": "2f2c4f4e-0222-4a3d-9c99-2d7e8a0c60b4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "2f2c4f4e-0222-4a3d-9c99-2d7e8a0c60b4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cec93466-86a1-4f8f-8bf9-5f1d53c4a208",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}