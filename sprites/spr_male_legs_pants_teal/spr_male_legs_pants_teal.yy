{
    "id": "c1cf8f81-53f1-4758-96b9-50fa60590e43",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_male_legs_pants_teal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1340,
    "bbox_left": 21,
    "bbox_right": 809,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e8d093cd-9e47-43de-96c9-407c57bd893d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1cf8f81-53f1-4758-96b9-50fa60590e43",
            "compositeImage": {
                "id": "56755479-34f7-4355-8957-ef300ed67a0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e8d093cd-9e47-43de-96c9-407c57bd893d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc459c48-010e-4132-9137-a7cd22b8613c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e8d093cd-9e47-43de-96c9-407c57bd893d",
                    "LayerId": "43144b7a-3586-4b86-a4a3-25b9accee518"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "43144b7a-3586-4b86-a4a3-25b9accee518",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c1cf8f81-53f1-4758-96b9-50fa60590e43",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}