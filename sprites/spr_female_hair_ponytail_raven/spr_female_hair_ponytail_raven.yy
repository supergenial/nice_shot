{
    "id": "67d8296b-9a57-4746-a8d1-8f5badeac3a7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_female_hair_ponytail_raven",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1343,
    "bbox_left": 18,
    "bbox_right": 812,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "92c18991-dada-43cc-8d1a-9dc5698c2cc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67d8296b-9a57-4746-a8d1-8f5badeac3a7",
            "compositeImage": {
                "id": "d4251e24-4929-4146-af46-17256f900fa5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92c18991-dada-43cc-8d1a-9dc5698c2cc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6e3ee29-973d-45f5-9f89-ba43e0b9c9af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92c18991-dada-43cc-8d1a-9dc5698c2cc3",
                    "LayerId": "2cfe0f93-0211-4664-848b-2ded4e2c51c1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "2cfe0f93-0211-4664-848b-2ded4e2c51c1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "67d8296b-9a57-4746-a8d1-8f5badeac3a7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}