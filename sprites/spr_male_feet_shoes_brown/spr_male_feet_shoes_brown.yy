{
    "id": "fed8db1d-6b54-4449-a528-060e33d143f0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_male_feet_shoes_brown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1341,
    "bbox_left": 19,
    "bbox_right": 810,
    "bbox_top": 54,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "afffdf2c-1839-4f26-9ca7-a135a4f2f0f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fed8db1d-6b54-4449-a528-060e33d143f0",
            "compositeImage": {
                "id": "1cf08ad8-08ce-40c0-809f-252e0653c5f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "afffdf2c-1839-4f26-9ca7-a135a4f2f0f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e920504d-02a8-4267-9f22-4b896ffdc307",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "afffdf2c-1839-4f26-9ca7-a135a4f2f0f3",
                    "LayerId": "4c0a67b0-c100-4a47-8a6d-5153b2cc63bc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "4c0a67b0-c100-4a47-8a6d-5153b2cc63bc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fed8db1d-6b54-4449-a528-060e33d143f0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}