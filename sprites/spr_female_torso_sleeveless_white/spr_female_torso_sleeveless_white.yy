{
    "id": "70653823-6e68-4b47-8b80-fbd9cfbab001",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_female_torso_sleeveless_white",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1336,
    "bbox_left": 23,
    "bbox_right": 807,
    "bbox_top": 34,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "263be1a1-189d-4b4a-9a4f-aca7cb6f308c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70653823-6e68-4b47-8b80-fbd9cfbab001",
            "compositeImage": {
                "id": "2319dcc3-3102-45bc-9f34-959668a98ea4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "263be1a1-189d-4b4a-9a4f-aca7cb6f308c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1d27dff-0238-48a7-b229-e8e54a0cac9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "263be1a1-189d-4b4a-9a4f-aca7cb6f308c",
                    "LayerId": "afa107cd-11f1-4e0d-9853-7d79f17064e1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "afa107cd-11f1-4e0d-9853-7d79f17064e1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70653823-6e68-4b47-8b80-fbd9cfbab001",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}