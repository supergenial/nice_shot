{
    "id": "71e9a1b7-e8f1-4b90-966d-9c24804c75f7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_female_legs_pants_teal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1341,
    "bbox_left": 24,
    "bbox_right": 807,
    "bbox_top": 45,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3a03da50-4d4a-46fc-880d-2469002c73ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71e9a1b7-e8f1-4b90-966d-9c24804c75f7",
            "compositeImage": {
                "id": "f6d2203f-aade-45c4-bc79-a077374e6f57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a03da50-4d4a-46fc-880d-2469002c73ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e303c9d-8e1b-49a7-8969-90192d1ecc67",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a03da50-4d4a-46fc-880d-2469002c73ee",
                    "LayerId": "c9f6b296-eb1f-4a76-866b-127af29dcd94"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "c9f6b296-eb1f-4a76-866b-127af29dcd94",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "71e9a1b7-e8f1-4b90-966d-9c24804c75f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}