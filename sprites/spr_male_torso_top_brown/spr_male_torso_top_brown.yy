{
    "id": "ba873981-30bd-4900-a1ac-cdd9637df8a7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_male_torso_top_brown",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1335,
    "bbox_left": 21,
    "bbox_right": 808,
    "bbox_top": 34,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6cf1a497-5885-4b23-b60c-82e4aae8beb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba873981-30bd-4900-a1ac-cdd9637df8a7",
            "compositeImage": {
                "id": "14fb6633-1f53-4d53-994d-b3b3062ff997",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cf1a497-5885-4b23-b60c-82e4aae8beb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edec483e-bb43-4ae0-b0ed-3d9dfe055a55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cf1a497-5885-4b23-b60c-82e4aae8beb8",
                    "LayerId": "6e5fb1d9-8c89-4832-a80a-7a23ac2cc61d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "6e5fb1d9-8c89-4832-a80a-7a23ac2cc61d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba873981-30bd-4900-a1ac-cdd9637df8a7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}