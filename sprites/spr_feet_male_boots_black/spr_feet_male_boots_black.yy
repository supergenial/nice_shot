{
    "id": "3d91c17e-ca80-416d-821d-a73281775cfc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_feet_male_boots_black",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1341,
    "bbox_left": 19,
    "bbox_right": 810,
    "bbox_top": 54,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d009ee7f-cbac-47af-b0d1-393497bfcc46",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d91c17e-ca80-416d-821d-a73281775cfc",
            "compositeImage": {
                "id": "e2bda1a2-95e7-49c6-a1cd-2f9e4a011cad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d009ee7f-cbac-47af-b0d1-393497bfcc46",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "947b401e-dad1-4ca6-a626-09186bb46c80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d009ee7f-cbac-47af-b0d1-393497bfcc46",
                    "LayerId": "e1093d51-f682-4528-b98e-5b142a78b4c8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1344,
    "layers": [
        {
            "id": "e1093d51-f682-4528-b98e-5b142a78b4c8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3d91c17e-ca80-416d-821d-a73281775cfc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 832,
    "xorig": 0,
    "yorig": 0
}